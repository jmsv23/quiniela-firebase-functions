const admin = require("firebase-admin");
const functions = require("firebase-functions");
const firestore = require("@google-cloud/firestore");
const client = new firestore.v1.FirestoreAdminClient();
const paypal = require("paypal-rest-sdk");
const payoutsNodeJssdk = require("@paypal/payouts-sdk");
const express = require("express");
const cors = require("cors");

let clientId =
  "AbP-xZNTqB-j7RpQTffF7q1Yx5lyWEoKV6AiFvr-mWJRUeTNv5gcexBcBmdVrs7pUenV1eRMzMu6tXEv";
let clientSecret =
  "EIo0ggYxnXgBmSX3Ji-L745L1wsnNZYGzd1mS7veVuyZReuTUi6xdI-hXtUzhevwcDlSuPrTYNoPypPe";
let environment = new payoutsNodeJssdk.core.SandboxEnvironment(
  clientId,
  clientSecret
);
let paypalPayoutClient = new payoutsNodeJssdk.core.PayPalHttpClient(
  environment
);

paypal.configure({
  mode: "sandbox", //sandbox or live
  client_id: clientId,
  client_secret: clientSecret
});

const app = express();

// Automatically allow cross-origin requests
app.use(cors({ origin: true }));

const axios = require("axios");
const _ = require("lodash");
const moment = require("moment");

admin.initializeApp();

var db = admin.firestore();
var dba = admin.database();

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//

const teamsAbbrs = {
  PUE: {
    machineName: "puebla",
    name: "Puebla"
  },
  VER: {
    machineName: "veracruz",
    name: "Veracruz"
  },
  MOR: {
    machineName: "m-morelia",
    name: "M. Morelia"
  },
  NEC: {
    machineName: "necaxa",
    name: "Necaxa"
  },
  LOB: {
    machineName: "lobos",
    name: "Lobos"
  },
  ATS: {
    machineName: "atlas",
    name: "Atlas"
  },
  JUA: {
    machineName: "juarez",
    name: "FC Juárez"
  },
  LEO: {
    machineName: "leon",
    name: "León"
  },
  QRO: {
    machineName: "queretaro",
    name: "Queretaro"
  },
  UAN: {
    machineName: "uanl",
    name: "UANL"
  },
  TOL: {
    machineName: "toluca",
    name: "Toluca"
  },
  AME: {
    machineName: "america",
    name: "América"
  },
  MON: {
    machineName: "monterrey",
    name: "Monterrey"
  },
  PUM: {
    machineName: "unam",
    name: "UNAM"
  },
  AUR: {
    machineName: "san-luis",
    name: "Atl. San Luis"
  },
  PAC: {
    machineName: "pachuca",
    name: "Pachuca"
  },
  CHI: {
    machineName: "guadalajara",
    name: "Guadalajara"
  },
  SAN: {
    machineName: "santos-laguna",
    name: "Santos Laguna"
  },
  TIJ: {
    machineName: "tijuana",
    name: "Tijuana"
  },
  CAZ: {
    machineName: "cruz-azul",
    name: "Cruz Azul"
  }
};

const testTokens = [
  "co9UjjV26jQ:APA91bFbVOTKYSadbVj6F1dRRB_ISOsqqwukM6mJKLZiI8UrrtYJI0RvuXELJAKYQgPHKpMCRDeUjAdv-3-IvjDfFDtbNHpt62wNsGVyrP-Pms-FvLiLIiZS6U4Pm5WzDCq2txVhmjV7",
  "ezTZDkyKKoU:APA91bHzB58n72bTpMb-3ml405NSCFWTnm5ExRU5FoqHHHff-dysdVJNH3Js-5YbREXVceOqCb9FZTAy-H6KyglHqg1UASYd5LCb6CNB3rQTlbGAOIlS7w6bEe_jbTShkf0c0n-QNKDv",
  "fjXSkUGZXXs:APA91bG7cZ_Qs_jRL16kzmD-M4MHbpnN5_YiMXDia-NUIqiqTAHmWYe6zHbZF9SJju1fhbd3z3rf1aPEV-3bIYinaqW9MSOaqTG-CetsnjkY7dqzeQGuXxj5oog9Qtx4QpS52EPIDg8-"
];

const testUID = "YW5D6oUNOnaQoL0TGwZH5AazqAG3";
const testQID = "JvKxr10E230P0g3ThmVt";

exports.sendTestNotifications = functions.https.onRequest((req, res) => {
  const message = {
    notification: {
      title: "Test title",
      body: `this notification it's a test to redirect to quiniela ${testQID}`
    },
    data: {
      qid: testQID
    },
    tokens: testTokens
  };
  admin
    .messaging()
    .sendMulticast(message)
    .then(response => {
      return res.status(200).send(response);
    })
    .catch(error => {
      return res.status(500).send(error);
    });
});

exports.quinielaUpdate = functions.firestore
  .document("quinielas/{qid}")
  .onUpdate((change, context) => {
    const id = change.after.id;
    const newValue = change.after.data();
    const previousValue = change.before.data();
    //Difference of requests;
    const diffRequests = _.difference(
      _.keys(newValue.requests),
      _.keys(previousValue.requests)
    );
    const diffPlayers = _.difference(
      _.keys(newValue.players),
      _.keys(previousValue.players)
    );
    if (diffRequests.length > 0 || diffPlayers.length > 0) {
      const uid = diffRequests.length > 0 ? newValue.owner : diffPlayers[0];
      const title =
        diffRequests.length > 0
          ? `Nueva solicitud en quiniela "${newValue.name}"`
          : `Ya eres parte de la quiniela "${newValue.name}"`;
      const body =
        diffRequests.length > 0
          ? `Un nuevo jugador quiere unirse a tu quiniela"${newValue.name}"`
          : `El administrador acepto tu solicitud en la quiniela "${newValue.name}", Suerte!!`;
      return dba
        .ref("tokens/" + uid)
        .once("value")
        .then(snapshot => {
          const tokens = Object.keys(snapshot.val());
          const message = {
            notification: {
              title,
              body
            },
            data: {
              qid: id
            },
            tokens
          };
          return admin.messaging().sendMulticast(message);
        })
        .then(response => console.log(response))
        .catch(err => console.log(err));
    } else {
      return true;
    }
  });

// exports.quinielaUpdate = functions.firestore
//   .document('quinielas/{qId}')
//   .onUpdate((change, context) => {
//     // Get an object representing the document
//     // e.g. {'name': 'Marie', 'age': 66}
//     const newValue = change.after.data();

//     // ...or the previous value before this update
//     const previousValue = change.before.data();

//     // access a particular field as you would any JS property
//     const name = newValue.name;

//     // perform desired operations ...
//     return null;
//   });

exports.generalNotifications = functions.https.onRequest((req, res) => {
  const url = "http://apiclient.resultados-futbol.com/scripts/api/api.php";
  const league = Number(req.query.league) || 141; //liga mx.
  const finals = req.query.finals || null;
  const leagueFinals = Number(req.query.leagueFinal) || 600; //liguilla mx.
  const apiKey = "7a33e49982af6f6d8c81c68efe01d851";
  let fullApiUrl = `${url}?key=${apiKey}&format=json&tz=America/Monterrey&league=${
    finals ? leagueFinals : league
  }&req=matchs`;

  axios
    .get(fullApiUrl)
    .then(response => {
      const match = response.data.match;
      const firstMatch = _.first(match);
      const iniciaJornada = moment(firstMatch.date, "YYYY/MM/DD").isSame(
        moment(),
        "day"
      );
      const todayMatches = _.filter(match, ma =>
        moment(ma.date, "YYYY/MM/DD").isSame(moment(), "day")
      );
      const notifications = [];
      if (iniciaJornada) {
        notifications.push({
          notification: {
            title: `Jornada ${firstMatch.round}`,
            body: `La jornada ${firstMatch.round} inicia hoy no te quedes sin meter tus resultados.`
          },
          topic: "general"
        });
      }
      if (todayMatches.length > 0) {
        _.forEach(todayMatches, m =>
          notifications.push({
            notification: {
              title: `${teamsAbbrs[m.local_abbr].name} vs ${
                teamsAbbrs[m.visitor_abbr].name
              }`,
              body: `${teamsAbbrs[m.local_abbr].name} vs ${
                teamsAbbrs[m.visitor_abbr].name
              } comienza a las ${m.hour +
                ":" +
                m.minute}, no olvides meter tus resultados para Suerte!`
            },
            topic: "general"
          })
        );
      }
      return notifications;
    })
    .then(notifications => {
      const n = [];
      _.forEach(notifications, nt => n.push(admin.messaging().send(nt)));
      return Promise.all(n);
    })
    // .then(response => res.status(200).send(response.successCount + ' messages were sent successfully'))
    .then(response => res.status(200).send(response))
    .catch(err => res.status(500).send(err));
});

exports.jornadas = functions.https.onRequest((req, res) => {
  const url = "http://apiclient.resultados-futbol.com/scripts/api/api.php";
  const round = req.query.round || null;
  const finals = req.query.finals || null;
  const apiKey = "7a33e49982af6f6d8c81c68efe01d851";
  const league = Number(req.query.league) || 68; //liga mx.
  const leagueFinals = Number(req.query.leagueFinal) || 1328; //liguilla mx.
  let jornada = {};
  let torneosRef = db.collection("torneos");
  let activeTorneo = torneosRef.where("league", "==", league);
  let fullApiUrl =
    url +
    "?key=" +
    apiKey +
    "&format=json&tz=America/Monterrey&league=" +
    (finals ? leagueFinals : league) +
    "&req=matchs";
  if (round) {
    fullApiUrl += "&round=" + round;
  }
  let id = (finals ? "l" : "") + "j";

  axios
    .get(fullApiUrl)
    .then(response => {
      const match = response.data.match;
      id += match[0].round;
      jornada = {
        start: moment(_.first(match).date, "YYYY/MM/DD").format("DD/MM/YYYY"),
        finish: moment(_.last(match).date, "YYYY/MM/DD")
          .add(1, "days")
          .format("DD/MM/YYYY"),
        name: (finals ? "Liguilla " : "") + "jornada " + _.first(match).round,
        partidos: {}
      };

      // Build the match object
      _.forEach(response.data.match, m => {
        const date = moment(
          `${m.date} ${m.hour}:${m.minute}`,
          "YYYY/MM/DD HH:mm"
        );
        const lScore = Number(m.local_goals);
        const vScore = Number(m.visitor_goals);
        const local = _.clone(
          _.assign(teamsAbbrs[m.local_abbr], {
            score: isNaN(lScore) ? 0 : lScore
          })
        );
        const visitante = _.clone(
          _.assign(teamsAbbrs[m.visitor_abbr], {
            score: isNaN(vScore) ? 0 : vScore
          })
        );
        const result = lScore > vScore ? "l" : lScore < vScore ? "v" : "e";
        jornada.partidos[m.id] = {
          local: local,
          visitante: visitante,
          date: date.format("DD/MM/YYYY"),
          hour: m.hour + ":" + m.minute,
          result: result,
          timestamp: date.format("X"),
          live_minute: m.live_minute,
          status: m.status
        };
      });
      return activeTorneo.get();
    })
    .then(snapshot => {
      const data = {};
      snapshot.forEach(doc => {
        const jornadas = _.clone(doc.data().jornadas);
        delete jornadas[id];
        jornadas[id] = jornada;
        data[doc.id] = jornadas;
      });
      return data;
    })
    .then(data => {
      const keys = _.keys(data);
      if (keys[0]) {
        const matchResults = {};
        Object.keys(data[keys[0]]).forEach(key => {
          matchResults[key] = {};
          Object.keys(data[keys[0]][key].partidos).forEach(km => {
            var partido = data[keys[0]][key].partidos[km];
            var validDate = moment(
              partido.date + " " + partido.hour,
              "DD/MM/YYYY HH:mm"
            ).isSameOrBefore(moment(), "minute");
            matchResults[key][km] = validDate ? partido.result : false;
          });
        });
        return torneosRef
          .doc(keys[0])
          .update({ jornadas: data[keys[0]], matchResults });
      } else {
        return "no tornament to update";
      }
    })
    .then(response => {
      return res.status(200).send(response);
    })
    .catch(error => {
      // handle error
      return res.status(500).send(error);
    });
});

exports.tabla = functions.firestore
  .document("torneos/{id}")
  .onUpdate((change, context) => {
    const previousValue = change.before.data();
    const apiKey = "7a33e49982af6f6d8c81c68efe01d851";
    const league = previousValue.league || 68; //liga mx.
    const url = "http://apiclient.resultados-futbol.com/scripts/api/api.php";
    let fullApiUrl =
      url +
      "?key=" +
      apiKey +
      "&format=json&tz=America/Monterrey&league=" +
      league +
      "&req=tables";

    const data = change.after.data();
    axios
      .get(fullApiUrl)
      .then(response => {
        const tabla = {};
        _.each(response.data.table, (value, key) => {
          tabla[key] = _.assign(value, teamsAbbrs[value.abbr]);
        });

        return change.after.ref.set(
          {
            tabla: tabla
          },
          { merge: true }
        );
      })
      .catch(err => {
        console.log("error", err);
      });
    return null;
  });

// Mensajes
exports.messageNotification = functions.database
  .ref("/messages/{qid}/{mid}")
  .onCreate((snapshot, context) => {
    // Grab the current value of what was written to the Realtime Database.
    const original = snapshot.val();
    console.log(context.params.qid, context.params.mid, original);
    return Promise.all([
      db
        .collection("quinielas")
        .doc(context.params.qid)
        .get(),
      dba.ref("tokens").once("value"),
      original,
      context.params.qid
    ])
      .then(data => {
        const quiniela = data[0].data();
        const usersKeys = _.keys(
          _.merge({ [quiniela.owner]: true }, quiniela.players)
        );
        const usersTokens = data[1].val();
        const tokens = {};
        usersKeys.forEach(i => {
          if (i !== data[2].uid) {
            _.merge(tokens, usersTokens[i]);
          }
        });
        const message = {
          notification: {
            title: `Nuevo mensaje de ${data[2].user.displayName}`,
            body: data[2].message
          },
          data: {
            qid: data[3],
            screenId: "mensajes"
          },
          tokens: _.keys(tokens)
        };
        return admin.messaging().sendMulticast(message);
      })
      .then(response => console.log(response))
      .catch(err => console.log(err));
  });

// Exportar datos.
const bucket = "gs://reapaldo-base-datos";
exports.scheduledFirestoreExport = functions.pubsub
  .schedule("every 72 hours")
  .onRun(context => {
    const databaseName = client.databasePath(
      process.env.GCP_PROJECT,
      "(default)"
    );
    console.log(databaseName);

    return client
      .exportDocuments({
        name: databaseName,
        outputUriPrefix: bucket,
        // Leave collectionIds empty to export all collections
        // or set to a list of collection IDs to export,
        // collectionIds: ['users', 'posts']
        collectionIds: []
      })
      .then(responses => {
        const response = responses[0];
        console.log(`Operation Name: ${response["name"]}`);
        return response;
      })
      .catch(err => {
        console.error(err);
        throw new Error("Export operation failed");
      });
  });
exports.writeInLog = functions.database
  .ref("/logs/{userId}/{logId}")
  .onCreate((snapshot, context) => {
    const userId = context.params.userId;
    const currentValue = snapshot.val();
    console.log("Usuario: ", context.params.userId, currentValue);
    dba.ref(`points/${userId}/points`).transaction(current_value => {
      let updateValue = 0;
      if (currentValue.operation === "add") {
        updateValue = (current_value || 0) + currentValue.value;
      } else {
        updateValue = current_value - currentValue.value;
      }
      return updateValue;
    });
  });

app.post("/api", (req, res) => {
  const { id, resource } = req.body;
  const { payer, purchase_units } = resource;
  const { email_address, payer_id } = payer;

  paypal.notification.webhookEvent.getAndVerify(
    JSON.stringify(req.body),
    (error, response) => {
      if (error) {
        console.log(error);
        throw error;
      } else {
        console.log("Simon", req.body.event_type);
        const paypalUserRef = db.collection("usersClabe");
        let query = paypalUserRef
          .where("email", "==", email_address)
          .get()
          .then(snapshot => {
            if (snapshot.empty) {
              console.log("No matching documents.");
              return;
            }
            console.log("Ultimo paso", req.body.event_type);
            snapshot.forEach(doc => {
              console.log(doc.id, "=>", doc.data());
              dba
                .ref("logs")
                .child(doc.id)
                .push({
                  operationId: id,
                  email: email_address,
                  value: Number(purchase_units[0].amount.value),
                  operation: "add",
                  payerID: payer_id,
                  date: moment()
                    .utcOffset("-06:00")
                    .format("MMMM Do YYYY"),
                });
            });
            return;
          })
          .catch(err => {
            console.log("Error getting documents", err);
          });
      }
    }
  );

  const date = new Date();
  const hours = (date.getHours() % 12) - 6; // MEX is UTC - 6hr;
  res.json({ bongs: "BONG ".repeat(hours) });
});
app.post("/payout", (req, res) => {
  const { amount, email, isDone } =  req.body;
  console.log("PAYOUT CALL ",amount, email, isDone,req.body);
  const paypalUserRef = db.collection("usersClabe");
  let query = paypalUserRef
    .where("email", "==", email)
    .get()
    .then(snapshot => {
      if (snapshot.empty) {
        console.log("No matching documents.");
        return;
      }
      console.log("Ultimo paso");
      snapshot.forEach(doc => {
        console.log(doc.id, "=>", doc.data());
        dba
          .ref("logs")
          .child(doc.id)
          .push({
            email: email,
            value: Number(amount),
            operation: "redeem",
            isDone,
            date: moment()
              .utcOffset("-06:00")
              .format("MMMM Do YYYY"),
          });
      });
      return;
    })
    .catch(err => {
      console.log("Error getting documents", err);
    });
  res.json({ bongs: "BONG " });
});
// Expose Express API as a single Cloud Function:
exports.luisTest = functions.https.onRequest(app);
